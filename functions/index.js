const functions = require('firebase-functions');
const request = require('request');
const config = {
    channelAccessToken: 'vYPNyQwSci7PwzkWuL+ZP4RS7+pOUmgghTf9cx1BChV6RqKeKhkWTTvCj7E+9MFg1r0L8JRaX6+MSRMZ+8FywCaKO6qki4Nd78ssd8iZlFEagYj/GCI1R/L8x8nIEBFKvA0VWmpfbmM9u6cGFeKDowdB04t89/1O/w1cDnyilFU=',
    channelSecret: '249234196cc5eb5dab50144eebc30a24',
  };
const express = require('express');
const line = require('@line/bot-sdk');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions

exports.helloWorld = functions.https.onRequest((request, response) => {
    response.send("Hello from Firebase!");
});

// create LINE SDK client
const client = new line.Client(config);

// create Express app
// about Express itself: https://expressjs.com/
const app = express();

// register a webhook handler with middleware
// about the middleware, please refer to doc
app.post('/', line.middleware(config), (req, res) => {
    res.sendStatus(200);
    Promise
        .all(req.body.events.map(handleEvent))
        .then((result) => res.json(result))
        .catch((err) => {
        console.error(err);
        res.status(500).end();
    });
});

// event handler
function handleEvent(event) {
  if (event.type !== 'message' || event.message.type !== 'text') {
    // ignore non-text-message event
    return Promise.resolve(null);
  }

  // create a echoing text message
  const echo = { type: 'text', text: event.message.text };

  // use reply API
  return client.replyMessage(event.replyToken, echo);
}


/*function reply(reply_token, msg) {
    let headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer vYPNyQwSci7PwzkWuL+ZP4RS7+pOUmgghTf9cx1BChV6RqKeKhkWTTvCj7E+9MFg1r0L8JRaX6+MSRMZ+8FywCaKO6qki4Nd78ssd8iZlFEagYj/GCI1R/L8x8nIEBFKvA0VWmpfbmM9u6cGFeKDowdB04t89/1O/w1cDnyilFU='
    }
    let body = JSON.stringify({
        replyToken: reply_token,
        messages: [{
            type: 'text',
            text: msg
        }]
    })
    request.post({
        url: 'https://api.line.me/v2/bot/message/reply',
        headers: headers,
        body: body
    }, (err, res, body) => {
        console.log('status = ' + res.statusCode);
    });
}

exports.webhook = functions.https.onRequest((request, response) => {
    let reply_token = request.body.events[0].replyToken
    let msg = request.body.events[0].message.text
    console.log(msg);
    reply(reply_token, msg);
    response.sendStatus(200);
});*/

exports.webhook = functions.https.onRequest(app);
